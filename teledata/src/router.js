import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import Register from '@/views/Register.vue'
import Admin from '@/views/Admin.vue'
import Charts from '@/views/Charts.vue'

Vue.use(Router)

const router = new Router({

    routes: [
      //The default edge should contain all the graphs. In this case, the 'Home' page.
      
      { path: '/charts', name: 'Charts', component: Charts  },
      { path: '/admin', name: 'Admin',  component: Admin  },
      { path: '/register', name: 'Register',  component: Register  }
    ]
})

export default router