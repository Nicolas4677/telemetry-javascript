// Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use( Vuex )

import TelemetryRecord from '@/../lib/TelemetryRecord'
import FireBaseConnection from '@/../lib/FireBase/FireBaseConnection'
import FireBaseDocument from '@/../lib/FireBase/FireBaseDocument'

const cloud = new FireBaseConnection();

const store = new Vuex.Store({ 
    state: {
        players: [],
        savedRecords: [],
        selectedPlayer: ""
    }, mutations: {

        ADD_PLAYER: (state, newPlayer) => {

            if(!state.players.includes(newPlayer)) {
                
                state.players.push(newPlayer);
                
            }
        },
        SET_RECORDS: ( state, records) => {

            state.savedRecords = [];

            records.forEach((doc) => {

                state.savedRecords.push(doc.data());
            });
        },
        SET_PLAYER:( state, playerId ) => {

            state.selectedPlayer = playerId;
        }
    }, actions: {

        setRecord({ commit }, data) {

            let newRecord = data.record;

            let newDocument = new FireBaseDocument("telemetry", data.playerId, cloud.dataBase)
            newDocument.initialize()
            .then(() => {

                return newDocument.addToSubcollection("Records", newRecord);
            })
            .catch((error) => {

                console.log(error)
            })
        },

        addPlayer({ commit }, newPlayer) {

            let newDocument = new FireBaseDocument("telemetry", newPlayer, cloud.dataBase)
            newDocument.initialize(new TelemetryRecord())
            .then(() => {

                commit('ADD_PLAYER', newPlayer);
            })
            .catch((error) => {
                console.log(error);
            })

        },
        refreshPlayers( { commit } ) {

            let db = cloud.dataBase;

            db.collection("telemetry").get().then((querySnapshot) => {

                querySnapshot.forEach((doc) => {

                    commit('ADD_PLAYER', doc.id);
                });
            });
        },
        refreshRecords( { commit } , playerId) {

            const collection = cloud.dataBase.collection("telemetry");
            const docRec = collection.doc(playerId);
            const subCollection = docRec.collection("Records");

            subCollection.get()
            .then((querySnapshot) => {

                commit('SET_RECORDS', querySnapshot);
            })
        },
        setPlayer( { commit }, playerId ) {

            commit('SET_PLAYER', playerId)
        }

    }, getters: {

        savedRecords: state => state.savedRecords,
        currentPlayers: state => state.players,
        selectedPlayer: state => state.selectedPlayer
    }})
export default store;