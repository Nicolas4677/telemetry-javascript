// Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

'use strict'

import { database } from "firebase";

export default class FireBaseDocument { 

    //documentId must be != ""
    constructor(parentCollection, documentId, dataBase) {
        
        this.parentCollection = parentCollection;
        this.documentId = documentId;
        this.data = {};
        this.document = null
        this.dataBase = dataBase;
    }

    initialize(data) {

        return new Promise(async (resolve, reject) => {

            this.getDocument(this.dataBase)
            .then((document) => {

                if(document.exists) {

                    this.document = document;
                    this.data = document.data();
                }
                else {
    
                    const collection = this.dataBase.collection(this.parentCollection);
                    let documentRef = collection.doc(this.documentId);
    
                    this.set(data, data.getDataConverter())
                }

                resolve();
            })
            .catch((error) => {

                reject(error);
            });
        });
    }

    getDocument() {

        const collection = this.dataBase.collection(this.parentCollection);
        let documentRef = collection.doc(this.documentId);

        return documentRef.get();
    }

    update(data = {}) {
        
        const collection = this.dataBase.collection(this.parentCollection);
        let documentRef = collection.doc(this.documentId);

        return documentRef.update(data);
    } 

    set(data = {}, dataConverter = {}) {
        const collection = this.dataBase.collection(this.parentCollection);
        let documentRef = collection.doc(this.documentId);

        return documentRef.withConverter(dataConverter).set(data);
    }

    addToSubcollection(collectionId, data) {

        const collection = this.dataBase.collection(this.parentCollection);
        const parentDocRef = collection.doc(this.documentId);
        const subCollection = parentDocRef.collection(collectionId);

        let count = 0;
        subCollection.get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                count++;
            });
                
            const subDocRef = subCollection.doc(`${++count}`);
            return subDocRef.withConverter(data.getDataConverter()).set(data)
        });
    }

    getSubcollection(collectionId) {

        const collection = this.dataBase.collection(this.parentCollection);
        const parentDocRef = collection.doc(this.documentId);
        const subCollection = parentDocRef.collection(collectionId);

        return subCollection.get();
    }
}