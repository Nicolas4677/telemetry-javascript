// Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

export default class TelemetryRecord {

    constructor(level = 0, kills = 0, deaths = 0) {
    
        this.level = level;
        this.kills = kills;
        this.deaths = deaths;
    }

    toString() {

        return `${this.level},${this.kills},${this.deaths}`;
    }

    getDataConverter() {

        let dataConverter = {
            
            toFirestore: function(record) {

                return {
                    level: record.level,
                    kills: record.kills,
                    deaths: record.deaths
                }
            },
            fromFirestore: function(snapshot, options) {

                const data = snapshot.data(options);
                return new TelemetryRecord(data.level, data.kills, data.deaths);
            }
        }

        return dataConverter;
    }
}